#-------------------------------------------------
#
# Project created by QtCreator 2018-06-22T21:26:42
#
#-------------------------------------------------

CONFIG += c++17
QMAKE_CXXFLAGS += -std=c++17

QT += core gui printsupport svg #webengine webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = InkJot
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    src/main.cpp \
    src/mainwindow.cpp \
    src/pentools.cpp \
    src/Model/textelement.cpp \
    src/Model/imageelement.cpp \
    src/Model/element.cpp \
    src/Model/page.cpp \
    src/pageview.cpp \
    src/resizewidget.cpp \
    src/Model/drawingelement.cpp \
    src/drawing.cpp

HEADERS  += \
    src/Model/element.hpp \
    src/mainwindow.hpp \
    src/Model/imageelement.hpp \
    src/Model/textelement.hpp \
    src/Model/page.hpp \
    src/pageview.hpp \
    src/resizewidget.hpp \
    src/pentools.hpp \
    src/Model/drawingelement.hpp \
    src/Model/insertdeletecommands.hpp \
    src/drawing.hpp

FORMS    += \
    ui/mainwindow.ui \
    ui/aboutbox.ui

RESOURCES += \
    resources/logo.qrc \
    resources/abouttext.qrc \
    resources/license.qrc \
    resources/rubbishbin.qrc
