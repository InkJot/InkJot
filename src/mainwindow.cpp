// mainwindow.cpp
// InkJot
// Copyright 2018 Jonathan Tanner and Joshua Smailes

// This file is part of InkJot.

// InkJot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// InkJot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with InkJot.  If not, see <https://www.gnu.org/licenses/>.

#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "ui_aboutbox.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    aboutBox(new QDialog),
    aboutBoxUi(new Ui::AboutBox),
    actionGroupImageScale(this) {

    ui->setupUi(this);

    refreshRecentFiles();
    if (recentPaths.empty()) {
        on_actionNew_triggered();
    } else {
        openPath(recentPaths[0].toString());
    }

    QSettings settings;
    QString workbookPath = settings.value("workbookPath", QVariant("")).toString();
    if (workbookPath == "") {
        ui->workbookView->hide();
    } else {
        openWorkbookPath(workbookPath);
    }

    QAction* actionUndo = ui->page->undoStack.createUndoAction(ui->menuEdit);
    QAction* actionRedo = ui->page->undoStack.createRedoAction(ui->menuEdit);
    actionUndo->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Z));
    actionRedo->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_Z));
    ui->menuEdit->insertAction(ui->actionUndo, actionUndo);
    ui->menuEdit->insertAction(ui->actionRedo, actionRedo);
    ui->menuEdit->removeAction(ui->actionUndo);
    ui->menuEdit->removeAction(ui->actionRedo);
    delete ui->actionUndo;
    delete ui->actionRedo;
    ui->actionUndo = actionUndo;
    ui->actionRedo = actionRedo;

    actionGroupImageScale.addAction(ui->actionFit);
    actionGroupImageScale.addAction(ui->actionFill);
    actionGroupImageScale.addAction(ui->actionStretch);
    actionGroupImageScale.setExclusive(true);

    aboutBoxUi->setupUi(aboutBox);
//    aboutBoxUi->logo->load(QString(":/logo.svg"));
    QFile aboutTextF(":/abouttext.html");
    if (!aboutTextF.open(QFile::ReadOnly | QFile::Text)) throw std::runtime_error("Cannot open abouttext.html");
    aboutBoxUi->contents->setHtml(QTextStream(&aboutTextF).readAll());
    QPalette palette = this->palette();
    palette.setColor(QPalette::ColorRole::Base, QColor(Qt::GlobalColor::transparent));
    aboutBoxUi->contents->setPalette(palette);
    //aboutBoxUi->contents->load("qrc:///abouttext.html");

    QObject::connect(ui->actionAbout, &QAction::triggered, aboutBox, &QDialog::show);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent* e) {
    if (showCloseDialog()) {
        e->accept();
    } else {
        e->ignore();
    }
}

void MainWindow::openPath(QString path) {
    ui->page->hide();
    delete ui->page;
    ui->page = new Page(path, ui->pageView);
    ui->pageView->setWidget(ui->page);
    ui->page->show();

    QSettings settings;
    QList<QVariant> recentPathsOld = settings.value("recentPaths", QVariant(QList<QVariant>())).toList();
    QList<QVariant> recentPathsNew = { QVariant(path) };
    for (int i = 0; i < 10 && !recentPathsOld.isEmpty();) {
        if (recentPathsOld.first().toString() != path) {
            recentPathsNew.push_back(recentPathsOld.first());
            i++;
        }
        recentPathsOld.removeFirst();
    }
    settings.setValue("recentPaths", QVariant(recentPathsNew));
    refreshRecentFiles();
}

void MainWindow::openWorkbookPath(QString path) {
    if (workbookModel != nullptr) {
        workbookModel->setParent(nullptr);
        delete workbookModel;
    }
    workbookModel = new QFileSystemModel(ui->workbookView);
    workbookModel->setNameFilterDisables(false);
    workbookModel->setNameFilters({ "*.ijn" });
    workbookModel->setRootPath(path);
    ui->workbookView->setModel(workbookModel);
    ui->workbookView->setRootIndex(workbookModel->index(path));
    for (int i = 1; i < workbookModel->columnCount(); i++) {
        ui->workbookView->setColumnHidden(i, true);
    }
    ui->workbookView->show();
    QSettings settings;

    int workbookWidth = settings.value("workbookWidth", QVariant(200)).toInt();
    ui->splitter->setSizes({ workbookWidth, width() - workbookWidth });

    settings.setValue("workbookPath", QVariant(path));
}

void MainWindow::on_actionNew_triggered() {
    if (showCloseDialog()) {
        ui->page->hide();
        delete ui->page;
        ui->pageView->setWidgetResizable(true);
        ui->page = new Page(ui->pageView);
        ui->pageView->setWidget(ui->page);
        ui->page->show();
        ui->pageView->setWidgetResizable(false);
    }
}

void MainWindow::on_actionOpen_triggered() {
    if (showCloseDialog()) {
        QString path = QFileDialog::getOpenFileName(this, "Open File", "", "InkJot notes (*.ijn)");
        if (path != "") {
            openPath(path);
        }
    }
}

void MainWindow::on_actionSave_triggered() {
    ui->page->save();
}

void MainWindow::on_actionSave_As_triggered() {
    QString path = QFileDialog::getSaveFileName(this, "Save File As", "", "InkJot notes (*.ijn)");
    if (path != "") {
        ui->page->path = path;
        ui->page->save();
    }
}

void MainWindow::on_actionOpen_Workbook_Folder_triggered() {
    QString path = QFileDialog::getExistingDirectory(this, "Open Workbook Folder", "");
    if (path != "") {
        openWorkbookPath(path);
    }
}

void MainWindow::on_actionExport_as_Image_triggered() {
    QString path = QFileDialog::getSaveFileName(this, "Export as Image", "", "Images (*.tiff *.png *.jpg)");
    if (path != "") {
        QImage image(ui->page->size(), QImage::Format_RGB888);
        ui->page->render(&image);
        image.save(path);
    }
}

void MainWindow::on_actionPrint_triggered() {
    QPrinter printer(QPrinter::PrinterMode::ScreenResolution);
    QPrintDialog printDialog(&printer, this);
    if (printDialog.exec() == QDialog::Accepted) {
        ui->page->render(&printer);
    }
}

void MainWindow::on_actionQuit_triggered() {
    close();
}

void MainWindow::on_splitter_splitterMoved(int pos, int index) {
    if (index == 1) {
        QSettings settings;
        if (pos == 0) {
            ui->workbookView->hide();
            if (workbookModel != nullptr) {
                workbookModel->setParent(nullptr);
                delete workbookModel;
                workbookModel = nullptr;
            }
            settings.remove("workbookPath");
            settings.remove("workbookWidth");
        } else {
            settings.setValue("workbookWidth", QVariant(pos));
        }
    }
}

void MainWindow::on_workbookView_activated(const QModelIndex &index) {
    if (showCloseDialog()) {
        openPath(workbookModel->filePath(index));
    }
}

void MainWindow::refreshRecentFiles() {
    for (QAction* action : ui->menuOpen_Recent->actions()) {
        ui->menuOpen_Recent->removeAction(action);
    }

    QSettings settings;
    recentPaths = settings.value("recentPaths", QVariant(QList<QVariant>())).toList();
    int i = 0;
    for (QVariant pathVariant : recentPaths) {
        QString path = pathVariant.toString();
        ui->menuOpen_Recent->addAction(path, [i, this]() { openPath(recentPaths[i].toString()); });
        i++;
    }
}

bool MainWindow::showCloseDialog() {
    if (ui->page->undoStack.isClean()) {
        return true;
    } else {
        QMessageBox closeDialog;
        closeDialog.setText("The document has been modified.");
        closeDialog.setInformativeText("Do you want to save your changes?");
        closeDialog.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        closeDialog.setDefaultButton(QMessageBox::Save);
        switch (closeDialog.exec()) {
        case QMessageBox::Save:
            on_actionSave_triggered();
            return true;
        case QMessageBox::Discard:
            return true;
        case QMessageBox::Cancel:
            return false;
        default:
            throw;
        }
    }
}
