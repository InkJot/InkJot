#include "page.hpp"
#include "insertdeletecommands.hpp"

Page::Page(QWidget* parent) :
    QFrame(parent) {
    setFrameShape(QFrame::Shape::StyledPanel);
    setAcceptDrops(true);
}

Page::Page(int width,
           int height,
           QWidget* parent) :
    Page(parent) {
    setGeometry(0, 0, width, height);
}

Page::Page(QXmlStreamReader* reader,
           QWidget *parent) :
    Page(parent) {
    reader->readNext();
    if (!reader->isStartDocument()) throw std::runtime_error("Reader not at start of a document");
    reader->readNext();
    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() != "page") throw std::runtime_error("Element is not a <page>");
    reader->readNext();

    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() != "size") throw std::runtime_error("Element is not a <size>");
    reader->readNext();
    if (reader->name() != "width") throw std::runtime_error("Element is not a <width>");
    int width = reader->readElementText().toInt();
    reader->readNext();
    if (reader->name() != "height") throw std::runtime_error("Element is not an <height>");
    int height = reader->readElementText().toInt();
    reader->readNext();
    reader->readNext();
    setGeometry(0, 0, width, height);

    while (reader->tokenType() == QXmlStreamReader::StartElement) {
        add(elementWithReader(reader, nextId++));
    }
    reader->readNext();
    if (!reader->isEndDocument()) throw std::runtime_error("Reader not reached end of document");
    delete reader;
}

Page::Page(QFile* file,
           QWidget *parent) :
    Page(new QXmlStreamReader(file),
         parent) {
    delete file;
}

QFile* fileWithPath(QString path) {
    QFile* file = new QFile(path);
    file->open(QIODevice::ReadOnly | QIODevice::Text);
    return file;
}

Page::Page(QString pathTMP,
           QWidget *parent) :
    Page(fileWithPath(pathTMP),
         parent) {
    path = pathTMP;
}

void Page::writeXML(QXmlStreamWriter* writer) {
    writer->writeStartElement("page");

    writer->writeStartElement("size");
    writer->writeTextElement("width", QString::number(geometry().width()));
    writer->writeTextElement("height", QString::number(geometry().height()));
    writer->writeEndElement(); //size

    for (Element* element : elements) {
        element->writeXML(writer);
    }
    writer->writeEndElement(); //page
}

void Page::save() {
    if (path == "") {
        path = QFileDialog::getSaveFileName(this, "Save File", "", "InkJot notes (*.ijn)");
    }
    if (path != "") {
        QFile* file = new QFile(path);
        file->open(QIODevice::WriteOnly | QIODevice::Text);
        QXmlStreamWriter* writer = new QXmlStreamWriter(file);
        writeXML(writer);
        file->write("\n");
        file->close();

        undoStack.setClean();
    }
}

void Page::add(Element* element) {
    QWidget* widget = dynamic_cast<QWidget*>(element);
    if (widget == nullptr) throw std::runtime_error("Element is not also a QWidget");
    widget->setParent(this);
    elements.push_back(element);
    for (Element* e : elements) {
        if (dynamic_cast<TextElement*>(e)) dynamic_cast<QWidget*>(e)->raise();
    }
}

void Page::remove(Element* element) {
    Element** it = std::find(elements.begin(), elements.end(), element);
    elements.erase(it);
}

void Page::setActive(bool active) {
    for (Element* element : elements) {
        element->setActive(active);
    }
}

void Page::setHeight(int height) {
    setGeometry(geometry().x(),
                geometry().y(),
                geometry().width(),
                height);
}

Element* Page::elementWithId(int id) {
    for (Element* element : elements) {
        if (element->id() == id) {
            return element;
        }
    }
    return nullptr;
}

QString Page::description() {
    QString result = "Page:\n";
    result += rectToString(geometry());
    for (Element* element : elements) {
        result += "\n" + element->description();
    }
    return result;
}

void Page::mousePressEvent(QMouseEvent* e) {
    setActive(false);
    QWidget::mousePressEvent(e);
}

void Page::mouseDoubleClickEvent(QMouseEvent* e) {
    setActive(false);
    undoStack.push(new InsertTextCommand(this, nextId, e->x(), e->y(), 100, 100, " "));
    TextElement* textElement = static_cast<TextElement*>(elementWithId(nextId++));
    textElement->setActive(true);
    textElement->moveCursor(QTextCursor::MoveOperation::Start, QTextCursor::MoveMode::MoveAnchor);
}

void Page::tabletEvent(QTabletEvent* e) {
    qDebug() << e->posF();
    if (activeDrawing == nullptr) {
        activeDrawing = new DrawingElement(e->x(), e->y(), 100, 100, nextId++);
        add(activeDrawing);
        activeDrawing->show();
        activeDrawing->setActive(true);
    }
    activeDrawing->addPoint(e->posF(), e->pressure());
}

void Page::dragEnterEvent(QDragEnterEvent* e) {
    if (e->mimeData()->hasUrls()) {
        QList<QUrl> urls = e->mimeData()->urls();
        for (QUrl url : urls) {
            QString path = url.toLocalFile();
            if (!QPixmap(path).isNull()) {
                if (e->source() == this) {
                    throw;
                    e->setDropAction(Qt::MoveAction);
                    e->accept();
                    return;
                } else {
                    e->acceptProposedAction();
                    return;
                }
            }
        }
    }
    if (e->mimeData()->hasImage()) {
        if (e->source() == this) {
            throw;
            e->setDropAction(Qt::MoveAction);
            e->accept();
            return;
        } else {
            e->acceptProposedAction();
            return;
        }
    }
    e->ignore();
}

void Page::dragMoveEvent(QDragMoveEvent* e) {
    if (e->mimeData()->hasUrls()) {
        QList<QUrl> urls = e->mimeData()->urls();
        for (QUrl url : urls) {
            QString path = url.toLocalFile();
            if (!QPixmap(path).isNull()) {
                if (e->source() == this) {
                    throw;
                    e->setDropAction(Qt::MoveAction);
                    e->accept();
                    return;
                } else {
                    e->acceptProposedAction();
                    return;
                }
            }
        }
    }
    if (e->mimeData()->hasImage()) {
        if (e->source() == this) {
            throw;
            e->setDropAction(Qt::MoveAction);
            e->accept();
            return;
        } else {
            e->acceptProposedAction();
            return;
        }
    }
    e->ignore();
}

void Page::dropEvent(QDropEvent* e) {
    if (e->mimeData()->hasUrls()) {
        QList<QUrl> urls = e->mimeData()->urls();
        bool shouldAccept = false;
        for (QUrl url : urls) {
            QString path = url.toLocalFile();
            if (!QPixmap(path).isNull()) {
                shouldAccept = true;
//                ImageElement* imageElement = new ImageElement(e->pos().x(), e->pos().y(), 500, 500, path, ImageElement::Scale::Fit);
//                add(imageElement);
//                imageElement->show();
                undoStack.push(new InsertImageCommand(this, nextId++, e->pos().x(), e->pos().y(), 500, 500, path, ImageElement::Scale::Fit));
            }
        }
        if (shouldAccept) {
            if (e->source() == this) {
                throw;
                e->setDropAction(Qt::MoveAction);
                e->accept();
                return;
            } else {
                e->acceptProposedAction();
                return;
            }
        }
    }
    if (e->mimeData()->hasImage()) {
        QImage image = qvariant_cast<QImage>(e->mimeData()->imageData());
        throw;
        if (e->source() == this) {
            throw;
            e->setDropAction(Qt::MoveAction);
            e->accept();
            return;
        } else {
            e->acceptProposedAction();
            return;
        }
    }
    e->ignore();
    update(geometry());
}

Element* elementWithReader(QXmlStreamReader* reader, int id, QWidget* parent) {
    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() == "text") {
        return new TextElement(reader, id, parent);
    } else if (reader->name() == "image") {
        return new ImageElement(reader, id, parent);
    } else if (reader->name() == "drawing") {
        return new DrawingElement(reader, id, parent);
    } else {
        throw std::runtime_error("Element not recognised");
    }
}
