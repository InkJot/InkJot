#include "imageelement.hpp"
#include "page.hpp"
#include "../resizewidget.hpp"
#include "../mainwindow.hpp"
#include "insertdeletecommands.hpp"
#include "ui_mainwindow.h"

ImageElement::ImageElement(int x,
                           int y,
                           int width,
                           int height,
                           QString src,
                           ImageElement::Scale scale,
                           int id,
                           QWidget* parent) :
    QLabel(parent),
    Element(id),
    src(src),
    scale(scale) {
    setGeometry(x, y, width, height);
    reloadImage();
}

ImageElement::ImageElement(QXmlStreamReader* reader,
                           int id,
                           QWidget *parent) :
    QLabel(parent),
    Element(id) {
    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() != "image") throw std::runtime_error("Element is not a <image>");
    QStringRef scaleString = reader->attributes().value("","scale");
    if (scaleString == "fit") scale = ImageElement::Scale::Fit;
    else if (scaleString == "fill") scale = ImageElement::Scale::Fill;
    else if (scaleString == "stretch") scale = ImageElement::Scale::Stretch;
    else throw std::runtime_error("Unrecognised scale value");
    reader->readNext();
    setGeometry(qRectFromReader(reader));
    if (reader->name() != "src") throw std::runtime_error("Element is not a <src>");
    src = reader->readElementText();
    reader->readNext();
    reader->readNext();
    reloadImage();
}

ImageElement::~ImageElement() {
//    Page* page = dynamic_cast<Page*>(parent());
//    page->undoStack.clear();
//    page->undoStack.resetClean();
    delete resizeWidget;
    delete rubbishBin;
}

void ImageElement::reloadImage() {
    QPixmap pixmap = QPixmap(src);
    switch (scale) {
    case Scale::Fit:
        pixmap = pixmap.scaled(geometry().size(), Qt::KeepAspectRatio);
        break;
    case Scale::Fill:
        pixmap = pixmap.scaled(geometry().size(), Qt::KeepAspectRatioByExpanding);
        break;
    case Scale::Stretch:
        pixmap = pixmap.scaled(geometry().size(), Qt::IgnoreAspectRatio);
        break;
    }
    setPixmap(pixmap);
}

void ImageElement::writeXML(QXmlStreamWriter* writer) {
    writer->writeStartElement("image");
    switch (scale) {
    case Scale::Fit:
        writer->writeAttribute("", "scale", "fit");
        break;
    case Scale::Fill:
        writer->writeAttribute("", "scale", "fill");
        break;
    case Scale::Stretch:
        writer->writeAttribute("", "scale", "stretch");
        break;
    }
    writeQRectWithWriter(geometry(), writer);
    writer->writeTextElement("src", src);
    writer->writeEndElement(); //image
}

void ImageElement::setActive(bool active) {
    Ui::MainWindow* ui = static_cast<MainWindow*>(QApplication::activeWindow())->ui;
    if (active) {
        if (resizeWidget == nullptr) {            
            resizeWidget = new ResizeWidget(ResizeHandle::Position::Left
                                          | ResizeHandle::Position::Right
                                          | ResizeHandle::Position::Top
                                          | ResizeHandle::Position::Bottom,
                                            id(),
                                            dynamic_cast<Page*>(parent()));
            QObject::connect(resizeWidget, &ResizeWidget::resizeCommand, [this](ResizeCommand* resizeCommand) {
                dynamic_cast<Page*>(parent())->undoStack.push(resizeCommand);
            });
        }
        ui->menuImage_Scale->setEnabled(true);
        switch (scale) {
        case Scale::Fit:
            ui->actionFit->trigger();
            break;
        case Scale::Fill:
            ui->actionFill->trigger();
            break;
        case Scale::Stretch:
            ui->actionStretch->trigger();
            break;
        }
        connectionFit = QObject::connect(ui->actionFit, &QAction::triggered, [this]() { scale = Scale::Fit; reloadImage(); });
        connectionFill = QObject::connect(ui->actionFill, &QAction::triggered, [this]() { scale = Scale::Fill; reloadImage(); });
        connectionStretch = QObject::connect(ui->actionStretch, &QAction::triggered, [this]() { scale = Scale::Stretch; reloadImage(); });
    } else {
        if (resizeWidget != nullptr) {
            delete resizeWidget;
            resizeWidget = nullptr;
        }
        ui->menuImage_Scale->setEnabled(false);
        QObject::disconnect(connectionFit);
        QObject::disconnect(connectionFill);
        QObject::disconnect(connectionStretch);
    }
}

QString ImageElement::description() {
    return "Image: " + src;
}

void ImageElement::mousePressEvent(QMouseEvent* e) {
    dragOffset = geometry().topLeft() - e->screenPos().toPoint();
    isDragging = false;

    preDragGeometry = geometry();
}

void ImageElement::mouseMoveEvent(QMouseEvent* e) {
    setGeometry(QRect(e->screenPos().toPoint() + dragOffset, geometry().size()));
    isDragging = true;
    if (resizeWidget != nullptr) {
        resizeWidget->recalculatePositions();
    }

    if (rubbishBin == nullptr) {
        QScrollArea* container = dynamic_cast<QScrollArea*>(parentWidget()->parentWidget()->parentWidget());
        rubbishBin = new QSvgWidget(":/rubbishbin/closed.svg", container);
        rubbishBin->move(container->rect().left(), container->rect().bottom() - rubbishBin->sizeHint().height() - container->horizontalScrollBar()->height());
        rubbishBin->show();
    }

    if (rubbishBin->geometry().contains(rubbishBin->parentWidget()->mapFromGlobal(e->screenPos().toPoint()))) {
        rubbishBin->load(QString(":/rubbishbin/open.svg"));
    } else {
        rubbishBin->load(QString(":/rubbishbin/closed.svg"));
    }
}

void ImageElement::mouseReleaseEvent(QMouseEvent* e) {
    Page* page = dynamic_cast<Page*>(parent());
    if (!isDragging) {
        page->setActive(false);
        setActive(true);
    } else {
        if (rubbishBin->geometry().contains(rubbishBin->parentWidget()->mapFromGlobal(e->screenPos().toPoint()))) {
            page->undoStack.push(new DeleteImageCommand(this, preDragGeometry));
        } else {
            page->undoStack.push(new MoveCommand(page, id(), preDragGeometry, geometry()));

            delete rubbishBin;
            rubbishBin = nullptr;
        }
    }
}

void ImageElement::keyPressEvent(QKeyEvent* e) {
    if (e->key() == Qt::Key_Delete || e->key() == Qt::Key_Backspace) {
        dynamic_cast<Page*>(parent())->remove(this);
        setParent(nullptr);
        delete this;
    } else {
        QLabel::keyPressEvent(e);
    }
}

void ImageElement::resizeEvent(QResizeEvent*) {
    reloadImage();
    if (resizeWidget != nullptr) {
        resizeWidget->recalculatePositions();
    }
}
