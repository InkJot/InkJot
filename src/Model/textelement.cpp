#include "textelement.hpp"
#include "page.hpp"
#include "../resizewidget.hpp"
#include "../mainwindow.hpp"
#include "insertdeletecommands.hpp"
#include "ui_mainwindow.h"

/** A command for editing the text in a text box */
class TextEditCommand : public QUndoCommand {
public:
    /** Construct a TextEditCommand
      * @param id           The id of the TextElement
      * @param page         The Page on which the TextElement appears
      * @param textPreEdit  The text before the edit
      * @param textPostEdit The text after the edit
      */
    TextEditCommand(int id,
                    Page* page,
                    QString textPreEdit,
                    QString textPostEdit) :
        id(id),
        page(page),
        textPreEdit(textPreEdit),
        textPostEdit(textPostEdit) {}

    virtual void undo() {
        TextElement* textElement = static_cast<TextElement*>(page->elementWithId(id));
        textElement->setPlainText(textPreEdit);
        textElement->recalculateHeight();
    }

    virtual void redo() {
        TextElement* textElement = static_cast<TextElement*>(page->elementWithId(id));
        textElement->setPlainText(textPostEdit);
        textElement->recalculateHeight();
    }
private:
    int id;
    Page* page;

    QString textPreEdit;
    QString textPostEdit;
};

/** A command for editing the font of a text box */
class ChangeTextFontCommand : public QUndoCommand {
public:
    /** Construct a ChangeTextFontCommand
      * @param id           The id of the TextElement
      * @param page         The Page on which the TextElement appears
      * @param fontPreEdit  The font before the edit
      * @param fontPostEdit The font after the edit
      */
    ChangeTextFontCommand(int id,
                          Page* page,
                          QFont fontPreEdit,
                          QFont fontPostEdit) :
        id(id),
        page(page),
        fontPreEdit(fontPreEdit),
        fontPostEdit(fontPostEdit) {}

    virtual void undo() {
        static_cast<TextElement*>(page->elementWithId(id))->setFont(fontPreEdit);
    }

    virtual void redo() {
        static_cast<TextElement*>(page->elementWithId(id))->setFont(fontPostEdit);
    }
private:
    int id;
    Page* page;

    QFont fontPreEdit;
    QFont fontPostEdit;
};

/** A command for editing the colour of a text box */
class ChangeTextColourCommand : public QUndoCommand {
public:
    /** Construct a ChangeTextColourCommand
      * @param id           The id of the TextElement
      * @param page         The Page on which the TextElement appears
      * @param colourPreEdit  The colour before the edit
      * @param colourPostEdit The colour after the edit
      */
    ChangeTextColourCommand(int id,
                            Page* page,
                            QColor colourPreEdit,
                            QColor colourPostEdit) :
        id(id),
        page(page),
        colourPreEdit(colourPreEdit),
        colourPostEdit(colourPostEdit) {}

    virtual void undo() {
        TextElement* textElement = static_cast<TextElement*>(page->elementWithId(id));
        QPalette palette = textElement->palette();
        palette.setColor(QPalette::ColorRole::Text, colourPreEdit);
        textElement->setPalette(palette);
    }

    virtual void redo() {
        TextElement* textElement = static_cast<TextElement*>(page->elementWithId(id));
        QPalette palette = textElement->palette();
        palette.setColor(QPalette::ColorRole::Text, colourPostEdit);
        textElement->setPalette(palette);
    }
private:
    int id;
    Page* page;

    QColor colourPreEdit;
    QColor colourPostEdit;
};

TextElement::TextElement(int id,
                         QWidget* parent) :
    QPlainTextEdit(parent),
    Element(id) {
    setReadOnly(true);
    setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);

    setFrameShape(QFrame::Shape::NoFrame);

    QPalette palette = this->palette();
    palette.setColor(QPalette::ColorRole::Base, QColor(Qt::GlobalColor::transparent));
    setPalette(palette);
}

TextElement::TextElement(int x,
                         int y,
                         int width,
                         int height,
                         QString body,
                         int id,
                         QWidget* parent) :
    TextElement(id, parent) {
    setPlainText(body);
    setGeometry(x, y, width, height);
}

TextElement::TextElement(int x,
                         int y,
                         int width,
                         int height,
                         QString body,
                         QFont font,
                         QColor colour,
                         int id,
                         QWidget* parent) :
    TextElement(x, y, width, height, body, id, parent) {
    setFont(font);
    QPalette palette = this->palette();
    palette.setColor(QPalette::ColorRole::Text, colour);
    setPalette(palette);
}

TextElement::TextElement(QXmlStreamReader* reader,
                         int id,
                         QWidget *parent) :
    TextElement(id, parent) {
    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() != "text") throw std::runtime_error("Element is not a <text>");
    reader->readNext();
    setGeometry(qRectFromReader(reader));
    if (reader->name() == "font") {
        setFont(qFontFromReader(reader));
    }
    if (reader->name() == "colour") {
        QPalette palette = this->palette();
        QColor colour = qColourFromReader(reader);
        palette.setColor(QPalette::ColorRole::Text, colour);
        setPalette(palette);
    }
    if (reader->name() != "body") throw std::runtime_error("Element is not a <body>");
    setPlainText(reader->readElementText());
    reader->readNext();
    reader->readNext();
}

TextElement::~TextElement() {
//    Page* page = dynamic_cast<Page*>(parent());
//    page->undoStack.clear();
//    page->undoStack.resetClean();
    delete resizeWidget;
}

void TextElement::writeXML(QXmlStreamWriter* writer) {
    writer->writeStartElement("text");
    writeQRectWithWriter(geometry(), writer);
    writeQFontWithWriter(font(), writer);
    writeQColourWithWriter(palette().color(QPalette::ColorRole::Text), writer);
    writer->writeTextElement("body", toPlainText());
    writer->writeEndElement(); //text
}

void TextElement::setActive(bool active) {
    Ui::MainWindow* ui = static_cast<MainWindow*>(QApplication::activeWindow())->ui;
    Page* page = dynamic_cast<Page*>(parent());
    if (active) {
        setReadOnly(false);
        setFrameShape(QFrame::Shape::StyledPanel);
        resizeWidget = new ResizeWidget(ResizeHandle::Position::Left | ResizeHandle::Position::Right, id(), page);
        QObject::connect(resizeWidget, &ResizeWidget::resizeCommand, [this, page](ResizeCommand* resizeCommand) {
            page->undoStack.push(resizeCommand);
        });

        preEditText = toPlainText();

        ui->actionFont->setEnabled(true);
        connectionFont = QObject::connect(ui->actionFont, &QAction::triggered, [this, page]() {
            QFont newFont = QFontDialog::getFont(nullptr, font(), this, "Select Font");
            page->undoStack.push(new ChangeTextFontCommand(id(), page, font(), newFont));
        });
        ui->actionColour->setEnabled(true);
        connectionColour = QObject::connect(ui->actionColour, &QAction::triggered, [this, page]() {
            QColor oldColour = palette().color(QPalette::ColorRole::Text);
            QColor newColour = QColorDialog::getColor(oldColour, this, "Select Font Colour");
            page->undoStack.push(new ChangeTextColourCommand(id(), page, oldColour, newColour));
        });
    } else {
        ui->actionFont->setEnabled(false);
        QObject::disconnect(connectionFont);
        ui->actionColour->setEnabled(false);
        QObject::disconnect(connectionColour);
        if (toPlainText() == "") {
            page->undoStack.push(new DeleteTextCommand(this, preEditText));
        } else {
            if (resizeWidget != nullptr) {
                delete resizeWidget;
                resizeWidget = nullptr;
            }
            setReadOnly(true);
            setFrameShape(QFrame::Shape::NoFrame);
            recalculateHeight();

            if (toPlainText() != preEditText && preEditText != "") {
                page->undoStack.push(new TextEditCommand(id(), page, preEditText, toPlainText()));
            }
            preEditText = "";
        }
    }
}

QString TextElement::description() {
    return "Text: " + toPlainText();
}

void TextElement::mousePressEvent(QMouseEvent* e) {
    if (isReadOnly()) {
        dragOffset = geometry().topLeft() - e->screenPos().toPoint();
        isDragging = false;

        preDragGeometry = geometry();
    } else {
        QPlainTextEdit::mousePressEvent(e);
    }
}

void TextElement::mouseMoveEvent(QMouseEvent* e) {
    if (isReadOnly()) {
        setGeometry(QRect(e->screenPos().toPoint() + dragOffset, geometry().size()));
        isDragging = true;
    } else {
        QPlainTextEdit::mouseMoveEvent(e);
    }
}

void TextElement::mouseReleaseEvent(QMouseEvent* e) {
    Page* page = dynamic_cast<Page*>(parent());
    if (isReadOnly()) {
        if (!isDragging) {
            page->setActive(false);
            setActive(true);
        } else {
            page->undoStack.push(new MoveCommand(page, id(), preDragGeometry, geometry()));
        }
    }
    QPlainTextEdit::mouseReleaseEvent(e);
}

void TextElement::keyPressEvent(QKeyEvent* e) {
    if (e->key() == Qt::Key_Escape && !isReadOnly()) {
        setActive(false);
    } else {
        QPlainTextEdit::keyPressEvent(e);
        recalculateHeight();
    }
}

void TextElement::resizeEvent(QResizeEvent* e) {
    recalculateHeight();
    QPlainTextEdit::resizeEvent(e);
    if (resizeWidget != nullptr) {
        resizeWidget->recalculatePositions();
    }
}

void TextElement::recalculateHeight() {
    QFont font = document()->defaultFont();
    QFontMetrics fontMetrics(font);
    QRect textRect = geometry().marginsRemoved(contentsMargins());
    int x = textRect.x();
    int y = textRect.y();
    int width = textRect.width();
    int height = textRect.height();
    QRect boundingRect = fontMetrics.boundingRect(x, y, width, INT_MAX, Qt::TextFlag::TextWordWrap | Qt::TextFlag::TextWrapAnywhere, toPlainText().append('\n'));
    height = boundingRect.height();
    textRect = QRect(x, y, width, height);
    textRect = textRect.marginsAdded(contentsMargins());
    setGeometry(textRect);
    verticalScrollBar()->setValue(0);
}

QFont qFontFromReader(QXmlStreamReader* reader) {
    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() != "font") throw std::runtime_error("Element is not a <font>");
    int pointSize = reader->attributes().value("", "pointSize").toInt();
    int weight = reader->attributes().value("", "weight").toInt();
    int italic = reader->attributes().value("", "italic") == "true";
    QString family = reader->readElementText();
    reader->readNext();
    return QFont(family, pointSize, weight, italic);
}

void writeQFontWithWriter(QFont font, QXmlStreamWriter* writer) {
    writer->writeStartElement("font");
    writer->writeAttribute("", "pointSize", QString::number(font.pointSize()));
    writer->writeAttribute("", "weight", QString::number(font.weight()));
    writer->writeAttribute("", "italic", font.italic() ? "true" : "false");
    writer->writeCharacters(font.family());
    writer->writeEndElement(); //font
}

QColor qColourFromReader(QXmlStreamReader* reader) {
    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() != "colour") throw std::runtime_error("Element is not a <colour>");
    reader->readNext();
    if (reader->name() != "red") throw std::runtime_error("Element is not a <red>");
    int red = reader->readElementText().toInt();
    reader->readNext();
    if (reader->name() != "green") throw std::runtime_error("Element is not a <green>");
    int green = reader->readElementText().toInt();
    reader->readNext();
    if (reader->name() != "blue") throw std::runtime_error("Element is not a <blue>");
    int blue = reader->readElementText().toInt();
    reader->readNext();
    reader->readNext();
    return QColor(red, green, blue);
}

void writeQColourWithWriter(QColor colour, QXmlStreamWriter* writer) {
    writer->writeStartElement("colour");
    writer->writeTextElement("", "red", QString::number(colour.red()));
    writer->writeTextElement("", "green", QString::number(colour.green()));
    writer->writeTextElement("", "blue", QString::number(colour.blue()));
    writer->writeEndElement(); //colour
}
