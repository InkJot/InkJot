#ifndef DRAWINGELEMENT_HPP
#define DRAWINGELEMENT_HPP

#include "element.hpp"
#include "../drawing.hpp"

#include <QWidget>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QPainterPath>
#include <QVector>

/** A drawing done with a graphics tablet */
class DrawingElement : public QWidget, public Element {
    Q_OBJECT
public:
    /** Construct a DrawingElement with the given position and size
      * @param x, y          The coordinates of the top left corner
      * @param width, height The size
      * @param id            A unique id that represents the element
      * @param parent        The parent widget
      */
    explicit DrawingElement(int x,
                            int y,
                            int width,
                            int height,
                            int id,
                            QWidget* parent = nullptr);
    /** Construct a DrawingElement with the given position and size
      * @param x, y          The coordinates of the top left corner
      * @param width, height The size
      * @param points        The initial array of points
      * @param id            A unique id that represents the element
      * @param parent        The parent widget
      */
    explicit DrawingElement(int x,
                            int y,
                            int width,
                            int height,
                            QVector<Drawing::Point> points,
                            int id,
                            QWidget* parent = nullptr);
    /** Construct a DrawingElement by reading an xml element
      * @param reader A QXmlStreamWriter object from which the xml data should be read
      * @param id     A unique id that represents the element
      * @param parent The parent widget
      */
    explicit DrawingElement(QXmlStreamReader* reader,
                            int id,
                            QWidget* parent = nullptr);

    virtual void writeXML(QXmlStreamWriter* writer) override;

    virtual void setActive(bool active) override;

    /** Add a point to the drawing
      * @param point    The coordinates of the point
      * @param pressure The pressure applied
      */
    void addPoint(QPointF point, qreal pressure);

    virtual QString description() override;
private:
    explicit DrawingElement(int id,
                            QWidget* parent = nullptr);

    QVector<Drawing::Point> points;
};

#endif // DRAWINGELEMENT_HPP
