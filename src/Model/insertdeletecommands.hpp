#ifndef INSERTDELETECOMMANDS_HPP
#define INSERTDELETECOMMANDS_HPP

#include "imageelement.hpp"
#include "textelement.hpp"
#include "page.hpp"

/** A command for inserting an image */
class InsertImageCommand : public QUndoCommand {
public:
    /** Construct an InsertImageCommand
      * @param page          The Page on which the ImageElement appears
      * @param id            The id of the ImageElement
      * @param x, y          The coordinates of the top left corner of the ImageElement
      * @param width, height The size of the ImageElement
      * @param path          The path to the image in the ImageElement
      * @param scale         How the image scales with the box
      */
    InsertImageCommand(Page* page,
                       int id,
                       int x,
                       int y,
                       int width,
                       int height,
                       QString path,
                       ImageElement::Scale scale) :
        page(page),
        id(id),
        x(x),
        y(y),
        width(width),
        height(height),
        path(path),
        scale(scale) {}

    virtual void undo() {
        Element* element = page->elementWithId(id);
        page->remove(element);
        delete element;
        element = nullptr;
    }

    virtual void redo() {
        ImageElement* element = new ImageElement(x, y, width, height, path, scale, id);
        page->add(element);
        element->show();
    }
private:
    Page* page;
    int id;
    int x;
    int y;
    int width;
    int height;
    QString path;
    ImageElement::Scale scale;
};

/** A command for deleting an image */
class DeleteImageCommand : public QUndoCommand {
public:
    /** Construct a DeleteImageCommand
      * @param element  The ImageElement
      * @param geometry The geometry of the ImageElement
      */
    DeleteImageCommand(ImageElement* element,
                       QRect geometry) :
        page(static_cast<Page*>(element->parent())),
        id(element->id()),
        x(geometry.x()),
        y(geometry.y()),
        width(geometry.width()),
        height(geometry.height()),
        path(element->src),
        scale(element->scale) {}

    virtual void undo() {
        ImageElement* element = new ImageElement(x, y, width, height, path, scale, id);
        page->add(element);
        element->show();
    }

    virtual void redo() {
        Element* element = page->elementWithId(id);
        page->remove(element);
        delete element;
    }
private:
    Page* page;
    int id;
    int x;
    int y;
    int width;
    int height;
    QString path;
    ImageElement::Scale scale;
};

/** A command for inserting a text box */
/** @todo sort out font and colour */
class InsertTextCommand : public QUndoCommand {
public:
    /** Construct an InsertTextCommand
      * @param page          The Page on which the TextElement appears
      * @param id            The id of the TextElement
      * @param x, y          The coordinates of the top left corner of the TextElement
      * @param width, height The size of the TextElement
      * @param body          The text in the TextElement
      */
    InsertTextCommand(Page* page,
                      int id,
                      int x,
                      int y,
                      int width,
                      int height,
                      QString body) :
        page(page),
        id(id),
        x(x),
        y(y),
        width(width),
        height(height),
        body(body),
        hasFontAndColour(false) {}
    /** Construct an InsertTextCommand
      * @param page          The Page on which the TextElement appears
      * @param id            The id of the TextElement
      * @param x, y          The coordinates of the top left corner of the TextElement
      * @param width, height The size of the TextElement
      * @param body          The text in the TextElement
      * @param font          The font of the text in the TextElement
      * @param colour        The colour of the text in the TextElement
      */
    InsertTextCommand(Page* page,
                      int id,
                      int x,
                      int y,
                      int width,
                      int height,
                      QString body,
                      QFont font,
                      QColor colour) :
        page(page),
        id(id),
        x(x),
        y(y),
        width(width),
        height(height),
        body(body),
        hasFontAndColour(true),
        font(font),
        colour(colour) {}

    virtual void undo() {
        Element* element = page->elementWithId(id);
        page->remove(element);
        delete element;
    }

    virtual void redo() {
        TextElement* element;
        if (hasFontAndColour) {
            element = new TextElement(x, y, width, height, body, font, colour, id);
        } else {
            element = new TextElement(x, y, width, height, body, id);
        }
        page->add(element);
        element->show();
    }
private:
    Page* page;
    int id;
    int x;
    int y;
    int width;
    int height;
    QString body;
    bool hasFontAndColour;
    QFont font;
    QColor colour;
};

/** A command for deleting a text box */
class DeleteTextCommand : public QUndoCommand {
public:
    /** Construct a DeleteTextCommand
      * @param element The TextElement
      * @param body    The text in the TextElement before deleting
      */
    DeleteTextCommand(TextElement* element,
                      QString body) :
        page(static_cast<Page*>(element->parent())),
        id(element->id()),
        x(element->x()),
        y(element->y()),
        width(element->width()),
        height(element->height()),
        body(body),
        font(element->font()),
        colour(element->palette().color(QPalette::ColorRole::Text)) {}

    virtual void undo() {
        TextElement* element = new TextElement(x, y, width, height, body, font, colour, id);
        page->add(element);
        element->show();
    }

    virtual void redo() {
        Element* element = page->elementWithId(id);
        page->remove(element);
        delete element;
    }
private:
    Page* page;
    int id;
    int x;
    int y;
    int width;
    int height;
    QString body;
    QFont font;
    QColor colour;
};

#endif // INSERTDELETECOMMANDS_HPP
