#ifndef PAGE_HPP
#define PAGE_HPP

#include <QtDebug>

#include "element.hpp"
#include "textelement.hpp"
#include "imageelement.hpp"
#include "drawingelement.hpp"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QObject>
#include <QWidget>
#include <QFile>
#include <QFileDialog>
#include <QMimeData>
#include <QSettings>
#include <QTabletEvent>
#include <QUndoStack>

#include <algorithm>

class PageView;

/** A page of notes */
class Page : public QFrame {
    friend PageView;
public:
    Q_OBJECT
public:
    /** Construct a page
      * @param parent The parent widget
      */
    explicit Page(QWidget* parent = nullptr);
    /** Construct a page with a given size
      * @param width, height The initial dimensions of the page
      * @param parent The parent widget
      */
    explicit Page(int width,
                  int height,
                  QWidget* parent = nullptr);
    /** Construct a page from the given xml
      * @param reader A QXmlStreamWriter object from which the xml data should be read
      * @param parent The parent widget
      */
    explicit Page(QXmlStreamReader* reader,
                  QWidget* parent = nullptr);
    /** Construct a page from the given file
      * @param file   A QFile object from which the data should be read
      * @param parent The parent widget
      */
    explicit Page(QFile* file,
                  QWidget* parent = nullptr);
    /** Construct a page from the given path
      * @param path   The path to the file from which the data should be read
      * @param parent The parent widget
      */
    explicit Page(QString path,
                  QWidget* parent = nullptr);

    /** Write this page as an xml element
      * @param writer A QXmlStreamWriter object to which the xml data should be written
      */
    void writeXML(QXmlStreamWriter* writer);

    /** Save the page to disk */
    void save();

    /** Add an element to the page
      * @param element The element to add
      */
    void add(Element* element);
    /** Remove an element from the page
      * @param element The element to remove
      */
    void remove(Element* element);

    /** Activate or disactivate all elements
      * @param active Whether the elements should be active
      */
    void setActive(bool active);

    /** Set the height of the page
      * @param height The new height of the page
      */
    void setHeight(int height);

    /** Get the element with a certain id
     * @param id The unique id of the desired element
     * @return   A pointer to the element
     */
    Element* elementWithId(int id);

    /** Get a description to be printed for debug purposes
      * @return The description without a trailing \\n
      */
    QString description();

    /** The path to the file if it has been saved, otherwise, an empty string */
    QString path = "";

    /** The undo stack for this page */
    QUndoStack undoStack;
protected:
    virtual void mousePressEvent(QMouseEvent* e) override;
    virtual void mouseDoubleClickEvent(QMouseEvent* e) override;
    virtual void tabletEvent(QTabletEvent* e) override;

    virtual void dragEnterEvent(QDragEnterEvent *event) override;
    virtual void dragMoveEvent(QDragMoveEvent *event) override;
    virtual void dropEvent(QDropEvent *event) override;
private:
    QVector<Element*> elements;

    DrawingElement* activeDrawing;

    int nextId = 0;
};

Element* elementWithReader(QXmlStreamReader* reader, int id, QWidget* parent = nullptr);

/** A command for moving an element */
class MoveCommand : public QUndoCommand {
public:
    /** Construct a ChangeTextColourCommand
      * @param id               The id of the Element
      * @param page             The Page on which the Element appears
      * @param preMoveGeometry  The geometry before the edit
      * @param postMoveGeometry The geometry after the edit
      */
    MoveCommand(Page* page,
                int id,
                QRect preMoveGeometry,
                QRect postMoveGeometry) :
        page(page),
        id(id),
        preMoveGeometry(preMoveGeometry),
        postMoveGeometry(postMoveGeometry) {}

    virtual void undo() {
        QWidget* widget = dynamic_cast<QWidget*>(page->elementWithId(id));
        widget->setGeometry(preMoveGeometry);
    }

    virtual void redo() {
        QWidget* widget = dynamic_cast<QWidget*>(page->elementWithId(id));
        widget->setGeometry(postMoveGeometry);
    }
private:
    Page* page;
    int id;
    QRect preMoveGeometry;
    QRect postMoveGeometry;
};

#endif // PAGE_HPP
