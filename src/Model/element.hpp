#ifndef ELEMENT_H
#define ELEMENT_H

#include <QtDebug>

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QObject>
#include <QWidget>
#include <QUndoCommand>

class ResizeWidget;

/** A pure virtual class that is a base class of all elements */
class Element {
public:
    /** Construct a new element with the given id
      * @param id
      * \parblock
      * A unique id that represents the element
      *
      * If the element is removed and then that same element restored the id must be the same
      * \endparblock
      */
    Element(int id) : _id(id) {}

    virtual ~Element() {} /**< Destruct the element */

    /** Write this element as an xml element
      * @param writer A QXmlStreamWriter object to which the xml data should be written
      */
    virtual void writeXML(QXmlStreamWriter* writer) = 0;

    /** Activate or disactivate the element
      * @param active Whether the element should be active
      */
    virtual void setActive(bool active) = 0;

    /** Get a description to be printed for debug purposes
      * @return The description without a trailing \\n
      */
    virtual QString description() = 0;

    /** Get the unique id that represents the element
      * @return The id
      */
    int id() { return _id; }

    /** A pointer to the ResizeWidget (if any) */
    ResizeWidget* resizeWidget = nullptr;
private:
    int _id;
};

QRect qRectFromReader(QXmlStreamReader* reader);
void writeQRectWithWriter(QRect rect, QXmlStreamWriter* writer);
QString rectToString(QRect rect);

#endif // ELEMENT_H
