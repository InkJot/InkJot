#include "element.hpp"

QRect qRectFromReader(QXmlStreamReader* reader) {
    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() != "rect") throw std::runtime_error("Element is not a <rect>");
    reader->readNext();
    if (reader->name() != "x") throw std::runtime_error("Element is not an <x>");
    int x = reader->readElementText().toInt();
    reader->readNext();
    if (reader->name() != "y") throw std::runtime_error("Element is not a <y>");
    int y = reader->readElementText().toInt();
    reader->readNext();
    if (reader->name() != "width") throw std::runtime_error("Element is not a <width>");
    int width = reader->readElementText().toInt();
    reader->readNext();
    if (reader->name() != "height") throw std::runtime_error("Element is not an <height>");
    int height = reader->readElementText().toInt();
    reader->readNext();
    reader->readNext();
    return QRect(x, y, width, height);
}

void writeQRectWithWriter(QRect rect, QXmlStreamWriter* writer) {
    writer->writeStartElement("rect");
    writer->writeTextElement("x", QString::number(rect.x()));
    writer->writeTextElement("y", QString::number(rect.y()));
    writer->writeTextElement("width", QString::number(rect.width()));
    writer->writeTextElement("height", QString::number(rect.height()));
    writer->writeEndElement(); //rect
}

QString rectToString(QRect rect) {
    QString result = "x: " + QString::number(rect.x());
    result += ", y: " + QString::number(rect.y());
    result += ", width: " + QString::number(rect.width());
    result += ", height: " + QString::number(rect.height());
    return result;
}
