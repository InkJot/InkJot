#ifndef TEXTELEMENT_HPP
#define TEXTELEMENT_HPP

#include "element.hpp"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QObject>
#include <QWidget>
#include <QPlainTextEdit>
#include <QPalette>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QResizeEvent>
#include <QFontMetrics>
#include <QScrollBar>
#include <QUndoCommand>
#include <QFontDialog>
#include <QColorDialog>

class TextEditCommand;
class ResizeWidget;

/** A box of text */
class TextElement : public QPlainTextEdit, public Element {
    Q_OBJECT
    friend TextEditCommand;
public:
    /** Construct a TextElement with the given position and size
      * @param x, y          The coordinates of the top left corner
      * @param width, height The size
      * @param body          The text in the box
      * @param id            A unique id that represents the element
      * @param parent        The parent widget
      */
    explicit TextElement(int x,
                         int y,
                         int width,
                         int height,
                         QString body,
                         int id,
                         QWidget* parent = nullptr);
    /** Construct a TextElement with the given position and size
      * @param x, y          The coordinates of the top left corner
      * @param width, height The size
      * @param body          The text in the box
      * @param font          The font in which the text should be displayed
      * @param colour        The colour of the text
      * @param id            A unique id that represents the element
      * @param parent        The parent widget
      */
    explicit TextElement(int x,
                         int y,
                         int width,
                         int height,
                         QString body,
                         QFont font,
                         QColor colour,
                         int id,
                         QWidget* parent = nullptr);
    /** Construct a TextElement by reading an xml element
      * @param reader A QXmlStreamWriter object from which the xml data should be read
      * @param id     A unique id that represents the element
      * @param parent The parent widget
      */
    explicit TextElement(QXmlStreamReader* reader,
                         int id,
                         QWidget* parent = nullptr);

    virtual ~TextElement();

    virtual void writeXML(QXmlStreamWriter* writer) override;

    virtual void setActive(bool active) override;

    virtual QString description() override;
protected:
    virtual void mousePressEvent(QMouseEvent* e) override;
    virtual void mouseMoveEvent(QMouseEvent* e) override;
    virtual void mouseReleaseEvent(QMouseEvent* e) override;
    virtual void keyPressEvent(QKeyEvent* e) override;
    virtual void resizeEvent(QResizeEvent* e) override;
    virtual void scrollContentsBy(int, int) override {}
private:
    explicit TextElement(int id,
                         QWidget* parent = nullptr);

    void recalculateHeight();

    QPoint dragOffset;
    bool isDragging;

    QString preEditText;
    QRect preDragGeometry = QRect();

    QMetaObject::Connection connectionFont;
    QMetaObject::Connection connectionColour;
};

QFont qFontFromReader(QXmlStreamReader* reader);
void writeQFontWithWriter(QFont font, QXmlStreamWriter* writer);

QColor qColourFromReader(QXmlStreamReader* reader);
void writeQColourWithWriter(QColor colour, QXmlStreamWriter* writer);

#endif // TEXTELEMENT_HPP
