#include "drawingelement.hpp"

DrawingElement::DrawingElement(int id,
                               QWidget* parent) :
    QWidget(parent),
    Element(id) {}

DrawingElement::DrawingElement(int x,
                               int y,
                               int width,
                               int height,
                               int id,
                               QWidget* parent) :
    DrawingElement(id, parent) {
    setGeometry(x, y, width, height);
}

DrawingElement::DrawingElement(int x,
                               int y,
                               int width,
                               int height,
                               QVector<Drawing::Point> pointsTMP,
                               int id,
                               QWidget* parent) :
    DrawingElement(x, y, width, height, id, parent) {
    points = pointsTMP;
}

DrawingElement::DrawingElement(QXmlStreamReader* reader,
                               int id,
                               QWidget *parent) :
    DrawingElement(id, parent) {
    if (!reader->isStartElement()) throw std::runtime_error("Reader not at start of an element");
    if (reader->name() != "drawing") throw std::runtime_error("Element is not a <drawing>");
    reader->readNext();
    setGeometry(qRectFromReader(reader));
    reader->readNext();
}

void DrawingElement::writeXML(QXmlStreamWriter* writer) {
    writer->writeStartElement("drawing");
    writeQRectWithWriter(geometry(), writer);
    writer->writeEndElement(); //drawing
}

void DrawingElement::setActive(bool /*active*/) {
}

void DrawingElement::addPoint(QPointF point, qreal pressure) {
    points.push_back(Drawing::Point(point, pressure));
}

QString DrawingElement::description() {
    return "Drawing";
}
