#ifndef IMAGEELEMENT_HPP
#define IMAGEELEMENT_HPP

#include "element.hpp"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QResizeEvent>
#include <QMetaObject>
#include <QSvgWidget>
#include <QScrollArea>
#include <QUndoCommand>

class DeleteImageCommand;
class ResizeWidget;

/** An image */
class ImageElement : public QLabel, public Element {
    Q_OBJECT
    friend DeleteImageCommand;
public:
    /** Options for how to scale the image with the box */
    enum class Scale {
        Fit,    /**< Make the image as large as possible to fit within the box */
        Fill,   /**< Make the image as small as possible to fill the whole box cropping it to fit */
        Stretch /**< Stretch the image, ignoring aspect ratio to fit and fill the box */
    };

    /** Construct an ImageElement with the given position and size
      * @param x, y          The coordinates of the top left corner
      * @param width, height The size
      * @param src           The path to the image file
      * @param scale         How the image should scale with the box
      * @param id            A unique id that represents the element
      * @param parent        The parent widget
      */
    explicit ImageElement(int x,
                          int y,
                          int width,
                          int height,
                          QString src,
                          Scale scale,
                          int id,
                          QWidget* parent = nullptr);
    /** Construct an ImageElement by reading an xml element
      * @param reader A QXmlStreamWriter object from which the xml data should be read
      * @param id     A unique id that represents the element
      * @param parent The parent widget
      */
    explicit ImageElement(QXmlStreamReader* reader,
                          int id,
                          QWidget* parent = nullptr);

    virtual ~ImageElement();

    virtual void writeXML(QXmlStreamWriter* writer) override;

    virtual void setActive(bool active) override;

    virtual QString description() override;
protected:
    virtual void mousePressEvent(QMouseEvent* e) override;
    virtual void mouseMoveEvent(QMouseEvent* e) override;
    virtual void mouseReleaseEvent(QMouseEvent* e) override;
    virtual void mouseDoubleClickEvent(QMouseEvent* e) override { e->ignore(); }
    virtual void keyPressEvent(QKeyEvent* e) override;
    virtual void resizeEvent(QResizeEvent*) override;
private:
    void reloadImage();

    QString src;
    Scale scale;

    QPoint dragOffset;
    bool isDragging;

    QRect preDragGeometry = QRect();

    QMetaObject::Connection connectionFit;
    QMetaObject::Connection connectionFill;
    QMetaObject::Connection connectionStretch;

    QSvgWidget* rubbishBin = nullptr;
};

#endif // IMAGEELEMENT_HPP
