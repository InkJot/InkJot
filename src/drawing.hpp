#ifndef LINE_HPP
#define LINE_HPP

#include <QtGui>
#include <QtMath>

namespace Drawing {

/** A point in a drawing */
struct Point {
    QPointF point;  /**< The position of the point */
    qreal radius; /**< The radius of the point */

    /** Construct a Point
     * @param point  The position of the point
     * @param radius The radius of the point
     */
    Point(QPointF point,
                 qreal radius) :
        point(point),
        radius(radius) {}

//    static QPainterPath join(QVector<DrawingPoint> points);

private:
    friend QVector<Point>;
    /** Construct an empty Point (used by QVector) */
    Point() {}
};

struct Line {
    QPointF start;
    QPointF end;

    Line(QPointF start, QPointF end);

    Line(Point start, Point end, bool isLeft);
};

}

#endif // LINE_HPP
