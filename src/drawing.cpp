#include "drawing.hpp"

qreal atan2(QPointF point) {
    return qAtan2(point.y(), point.x());
}

//QPainterPath Drawing::Point::join(QVector<Point> points) {
//    QPainterPath path;
//    Point currentPoint = points.first();
//    for (const Point point : points.mid(1)) {
//        currentPoint = point;
//    }
//}

Drawing::Line::Line(QPointF start, QPointF end) :
    start(start),
    end(end) {}

Drawing::Line::Line(Point startTMP, Point endTMP, bool isLeft) :
    Line(startTMP.point, endTMP.point) {
    qreal angle = atan2(end - start);
    if (isLeft) angle += M_PI_2;
    else angle += M_PI + M_PI_2;
    QPointF offset = QPointF(qCos(angle), qSin(angle));
    start += startTMP.radius * offset;
    end += endTMP.radius * offset;
}
