#ifndef RESIZEWIDGET_HPP
#define RESIZEWIDGET_HPP

#include "Model/page.hpp"

#include <QWidget>
#include <QPushButton>
#include <QMouseEvent>
#include <QUndoCommand>

QSize operator +(QSize lhs, QPoint rhs);
QSize operator -(QSize lhs, QPoint rhs);
QPoint operator +(QPoint lhs, QSize rhs);
QPoint operator -(QPoint lhs, QSize rhs);

class ResizeWidget;
class ResizeCommand;

/** A handle to resize a widget */
class ResizeHandle : public QPushButton {
    Q_OBJECT
    friend ResizeWidget;
public:
    /** The position around the widget which the handle should take */
    enum Position {
        Left = 1u << 0,
        Right = 1u << 1,
        Top = 1u << 2,
        Bottom = 1u << 3,
        TopLeft = Top | Left,
        TopRight = Top | Right,
        BottomLeft = Bottom | Left,
        BottomRight = Bottom | Right,
    };

    /** Construct a ResizeHandle for the given widget
      * @param position The position in which this handle should be
      * @param id       The id of the Element
      * @param page     The Page on which the Element appears
      */
    explicit ResizeHandle(Position position, int id, Page* page);
protected:
    virtual void mousePressEvent(QMouseEvent* e);
    virtual void mouseMoveEvent(QMouseEvent* e);
    virtual void mouseReleaseEvent(QMouseEvent* e);
private:
    void recalculatePosition();

    Position position;

    int id;
    Page* page;

    QPointF dragLast;

    QRect preDragGeometry = QRect();
};

/** A widget containing ResizeHandles around an object */
class ResizeWidget : public QObject {
    Q_OBJECT
    friend ResizeHandle;
public:
    /** Construct a ResizeWidget for the given widget
      * @param positions A bitmask of Positions representing the handles to include
      * @param id        The id of the Element
      * @param page      The Page on which the Element appears
      */
    explicit ResizeWidget(unsigned int positions, int id, Page* page);
    /** Destruct the ResizeWidget */
    ~ResizeWidget();

    /** Update the positions of the handles */
    void recalculatePositions();
signals:
    /** Emitted when a handle is dragged
      * @param resizeCommand A command that can be used to undo the resize
      */
    void resizeCommand(ResizeCommand* resizeCommand);
private:
    unsigned int positions;

    ResizeHandle* leftHandle;
    ResizeHandle* rightHandle;
    ResizeHandle* topHandle;
    ResizeHandle* bottomHandle;
    ResizeHandle* topLeftHandle;
    ResizeHandle* topRightHandle;
    ResizeHandle* bottomLeftHandle;
    ResizeHandle* bottomRightHandle;
};

/** A command for resizing a widget */
class ResizeCommand : public QUndoCommand {
public:
    /** Construct a ResizeCommand
      * @param id                 The id of the Element
      * @param page               The Page on which the Element appears
      * @param preResizeGeometry  The geometry before the resize
      * @param postResizeGeometry The geometry after the resize
      */
    ResizeCommand(int id,
                  Page* page,
                  QRect preResizeGeometry,
                  QRect postResizeGeometry) :
        id(id),
        page(page),
        preResizeGeometry(preResizeGeometry),
        postResizeGeometry(postResizeGeometry) {}

    virtual void undo() {
        dynamic_cast<QWidget*>(page->elementWithId(id))->setGeometry(preResizeGeometry);
    }

    virtual void redo() {
        dynamic_cast<QWidget*>(page->elementWithId(id))->setGeometry(postResizeGeometry);
    }
private:
    int id;
    Page* page;

    QRect preResizeGeometry;
    QRect postResizeGeometry;
};

#endif // RESIZEWIDGET_HPP
