#include "pageview.hpp"

PageView::PageView(QWidget* parent) :
    QScrollArea(parent) {}

void PageView::scrollContentsBy(int dx, int dy) {
    Page* page = dynamic_cast<Page*>(widget());
    if (page) {
        QScrollArea::scrollContentsBy(dx, dy);
        int elementsBottom = 0;
        for (Element* element : page->elements) {
            QWidget* widget = dynamic_cast<QWidget*>(element);
            elementsBottom = std::max(widget->geometry().bottom(), elementsBottom);
        }
        page->setHeight(std::max(elementsBottom,
                                 verticalScrollBar()->sliderPosition() + rect().height() + 100));
    }
}
