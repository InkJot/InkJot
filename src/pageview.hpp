#ifndef PAGEVIEW_H
#define PAGEVIEW_H

#include "Model/page.hpp"

#include <QScrollArea>
#include <QScrollBar>
#include <QWidget>

/** A widget containing a page that controls scrolling */
class PageView : public QScrollArea {
public:
    /** Construct a PageView
      * @param parent The parent widget
      */
    PageView(QWidget* parent = nullptr);
protected:
    virtual void scrollContentsBy(int dx, int dy) override;
};

#endif // PAGEVIEW_H
