#include "resizewidget.hpp"

QSize operator +(QSize lhs, QPoint rhs) {
    return QSize(lhs.width() + rhs.x(), lhs.height() + rhs.y());
}
QSize operator -(QSize lhs, QPoint rhs) {
    return QSize(lhs.width() - rhs.x(), lhs.height() - rhs.y());
}
QPoint operator +(QPoint lhs, QSize rhs) {
    return QPoint(lhs.x() + rhs.width(), lhs.y() + rhs.height());
}
QPoint operator -(QPoint lhs, QSize rhs) {
    return QPoint(lhs.x() - rhs.width(), lhs.y() - rhs.height());
}

bool contains(unsigned int lhs, unsigned int rhs) {
    return (lhs & rhs) == rhs;
}

ResizeHandle::ResizeHandle(Position position, int id, Page* page) :
    QPushButton(dynamic_cast<QWidget*>(page->elementWithId(id))->parentWidget()),
    position(position),
    id(id),
    page(page) {
    resize(10, 10);
    recalculatePosition();
    raise();
    show();
}

void ResizeHandle::mousePressEvent(QMouseEvent* e) {
    dragLast = e->screenPos();
    preDragGeometry = dynamic_cast<QWidget*>(page->elementWithId(id))->geometry();
}

void ResizeHandle::mouseMoveEvent(QMouseEvent* e) {
    QWidget* widget = dynamic_cast<QWidget*>(page->elementWithId(id));
    QPoint dragOffset = (e->screenPos() - dragLast).toPoint();
    if (position & Position::Left) {
        widget->resize(widget->width() - dragOffset.x(), widget->height());
        widget->move(widget->x() + dragOffset.x(), widget->y());
    }
    if (position & Position::Right) {
        widget->resize(widget->width() + dragOffset.x(), widget->height());
    }
    if (position & Position::Top) {
        widget->resize(widget->width(), widget->height() - dragOffset.y());
        widget->move(widget->x(), widget->y() + dragOffset.y());
    }
    if (position & Position::Bottom) {
        widget->resize(widget->width(), widget->height() + dragOffset.y());
    }
    page->elementWithId(id)->resizeWidget->recalculatePositions();
    dragLast = e->screenPos();
}

void ResizeHandle::mouseReleaseEvent(QMouseEvent*) {
    emit page->elementWithId(id)->resizeWidget->resizeCommand(new ResizeCommand(id, page, preDragGeometry, dynamic_cast<QWidget*>(page->elementWithId(id))->geometry()));
}

void ResizeHandle::recalculatePosition() {
    QWidget* widget = dynamic_cast<QWidget*>(page->elementWithId(id));
    move(widget->geometry().center() - (size() / 2));
    if (position & Position::Left) {
        move(widget->geometry().left() - (width() / 2), y());
    }
    if (position & Position::Right) {
        move(widget->geometry().right() - (width() / 2), y());
    }
    if (position & Position::Top) {
        move(x(), widget->geometry().top() - (height() / 2));
    }
    if (position & Position::Bottom) {
        move(x(), widget->geometry().bottom() - (height() / 2));
    }
}

ResizeWidget::ResizeWidget(unsigned int positions,
                           int id,
                           Page* page) :
    positions(positions) {
    if (contains(positions, ResizeHandle::Position::Left))
        leftHandle = new ResizeHandle(ResizeHandle::Position::Left, id, page);
    if (contains(positions, ResizeHandle::Position::Right))
        rightHandle = new ResizeHandle(ResizeHandle::Position::Right, id, page);
    if (contains(positions, ResizeHandle::Position::Top))
        topHandle = new ResizeHandle(ResizeHandle::Position::Top, id, page);
    if (contains(positions, ResizeHandle::Position::Bottom))
        bottomHandle = new ResizeHandle(ResizeHandle::Position::Bottom, id, page);
    if (contains(positions, ResizeHandle::Position::TopLeft))
        topLeftHandle = new ResizeHandle(ResizeHandle::Position::TopLeft, id, page);
    if (contains(positions, ResizeHandle::Position::TopRight))
        topRightHandle = new ResizeHandle(ResizeHandle::Position::TopRight, id, page);
    if (contains(positions, ResizeHandle::Position::BottomLeft))
        bottomLeftHandle = new ResizeHandle(ResizeHandle::Position::BottomLeft, id, page);
    if (contains(positions, ResizeHandle::Position::BottomRight))
        bottomRightHandle = new ResizeHandle(ResizeHandle::Position::BottomRight, id, page);
}

ResizeWidget::~ResizeWidget() {
    if (contains(positions, ResizeHandle::Position::Left))
        leftHandle->setParent(nullptr);
    if (contains(positions, ResizeHandle::Position::Right))
        rightHandle->setParent(nullptr);
    if (contains(positions, ResizeHandle::Position::Top))
        topHandle->setParent(nullptr);
    if (contains(positions, ResizeHandle::Position::Bottom))
        bottomHandle->setParent(nullptr);
    if (contains(positions, ResizeHandle::Position::TopLeft))
        topLeftHandle->setParent(nullptr);
    if (contains(positions, ResizeHandle::Position::TopRight))
        topRightHandle->setParent(nullptr);
    if (contains(positions, ResizeHandle::Position::BottomLeft))
        bottomLeftHandle->setParent(nullptr);
    if (contains(positions, ResizeHandle::Position::BottomRight))
        bottomRightHandle->setParent(nullptr);
}

void ResizeWidget::recalculatePositions() {
    if (contains(positions, ResizeHandle::Position::Left))
        leftHandle->recalculatePosition();
    if (contains(positions, ResizeHandle::Position::Right))
        rightHandle->recalculatePosition();
    if (contains(positions, ResizeHandle::Position::Top))
        topHandle->recalculatePosition();
    if (contains(positions, ResizeHandle::Position::Bottom))
        bottomHandle->recalculatePosition();
    if (contains(positions, ResizeHandle::Position::TopLeft))
        topLeftHandle->recalculatePosition();
    if (contains(positions, ResizeHandle::Position::TopRight))
        topRightHandle->recalculatePosition();
    if (contains(positions, ResizeHandle::Position::BottomLeft))
        bottomLeftHandle->recalculatePosition();
    if (contains(positions, ResizeHandle::Position::BottomRight))
        bottomRightHandle->recalculatePosition();
}
