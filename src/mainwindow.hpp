// mainwindow.h
// InkJot
// Copyright 2018 Jonathan Tanner and Joshua Smailes

// This file is part of InkJot.

// InkJot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// InkJot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with InkJot.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtDebug>

#include "Model/page.hpp"
#include <QMainWindow>
#include <QWidget>
#include <QFileDialog>
#include <QList>
#include <QVariant>
#include <QAction>
#include <QFileSystemModel>
#include <QImage>
#include <QPrinter>
#include <QPrintDialog>
#include <QCloseEvent>
#include <QMessageBox>
#include <QActionGroup>
#include <QDialog>
#include <QSvgWidget>
#include <QFile>
#include <QTextStream>

namespace Ui {
class MainWindow;
class AboutBox;
}

/** The main window */
class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    /** Construct a MainWindow
      * @param parent The parent widget
      */
    explicit MainWindow(QWidget* parent = 0);

    /** Destruct the MainWindow */
    ~MainWindow();

    /** A pointer to the UI */
    Ui::MainWindow* ui;
protected:
    virtual void closeEvent(QCloseEvent* e);

private slots:
    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionSave_As_triggered();
    void on_actionOpen_Workbook_Folder_triggered();
    void on_actionExport_as_Image_triggered();
    void on_actionPrint_triggered();
    void on_actionQuit_triggered();

    void on_splitter_splitterMoved(int pos, int index);
    void on_workbookView_activated(const QModelIndex &index);

private:
    void openPath(QString path);
    void openWorkbookPath(QString path);

    void refreshRecentFiles();

    bool showCloseDialog();

    QDialog* aboutBox;
    Ui::AboutBox* aboutBoxUi;

    QList<QVariant> recentPaths;

    QFileSystemModel* workbookModel = nullptr;

    QActionGroup actionGroupImageScale;
};

#endif // MAINWINDOW_H
