#include <iostream>
#include <cstdlib>

typedef std::uint8_t BYTE;
typedef std::uint16_t WORD;
typedef std::uint32_t DWORD;
typedef std::uint64_t QWORD;

struct A {
  int a;
  int x[5];
};

#pragma pack(push, 1)
struct GUID {
  DWORD d1;
  WORD d2;
  WORD d3;
  BYTE d4[8];
};
#pragma pack(pop)

int main() {
  std::cerr << sizeof(A) << " " << sizeof(A::x) << std::endl;
  std::cerr << sizeof(GUID) << " " << sizeof(GUID::d4) << std::endl;
}

